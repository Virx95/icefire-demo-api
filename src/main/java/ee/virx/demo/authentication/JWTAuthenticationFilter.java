package ee.virx.demo.authentication;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;

import javax.security.sasl.AuthenticationException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    @Value("${authentication.header.token.name}")
    private String tokenHeaderName;

    private TokenService tokenService;

    public JWTAuthenticationFilter(TokenService tokenService) {
        this.tokenService = tokenService;
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        var httpRequest = (HttpServletRequest) request;
        var authToken = httpRequest.getHeader(this.tokenHeaderName);
        if (authToken != null && authToken.length() >= 7) {
            authToken = tokenService.removeBearerPrefixFromToken(authToken);
            var user = tokenService.getUserFromTokenInfo(authToken);
            if (user.isPresent() && authenticationDataIsNotSetInSecurityContext()) {
                UserDetails userDetails = user.get();
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                                userDetails, null, userDetails.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpRequest));
                injectTokenDataIntoSecurityContext(authentication);
            }
        }
        chain.doFilter(request, response);
    }

    private boolean authenticationDataIsNotSetInSecurityContext() {
        return SecurityContextHolder.getContext().getAuthentication() == null;
    }

    private void injectTokenDataIntoSecurityContext(UsernamePasswordAuthenticationToken authentication) {
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
