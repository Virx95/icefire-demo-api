package ee.virx.demo.authentication;

import ee.virx.demo.controller.vm.LoginResponseVM;
import ee.virx.demo.domain.user.User;
import ee.virx.demo.domain.user.UserRepository;
import io.jsonwebtoken.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.util.Base64;
import java.util.Date;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Service
public class TokenService {

    private String secretKey;
    private UserRepository userRepository;
    private SecretKey JWTS_KEY;

    public TokenService(UserRepository userRepository,
                        @Value("${authentication.secret_key}") String secretKey) {
        this.userRepository = userRepository;
        this.secretKey = secretKey;
    }

    @PostConstruct
    public void setJWTSKey() {
        byte[] decodedKey = Base64.getDecoder().decode(secretKey);
        this.JWTS_KEY = new SecretKeySpec(decodedKey, 0, decodedKey.length, SignatureAlgorithm.HS256.getValue());
    }

    private String generateToken(long id, long secondsToExpire) {
        Date now = new Date(System.currentTimeMillis());
        Date dateLimit = new Date(now.getTime() + secondsToExpire * 1000);
        return Jwts.builder()
                .setSubject(Long.toString(id))
                .setIssuedAt(now)
                .setExpiration(dateLimit)
                .signWith(SignatureAlgorithm.HS256, JWTS_KEY)
                .compact();
    }

    public LoginResponseVM generateTokenResponse(User user) {
        return new LoginResponseVM(generateToken(user.getId(), TimeUnit.HOURS.toSeconds(8L)));
    }

    public Optional<User> getUserFromTokenInfo(String authToken) {
        return getTokenClaims(authToken).flatMap(subject -> userRepository.findById(Long.parseLong(subject.getSubject())));
    }

    public Optional<Claims> getTokenClaims(String authToken) {
        try {
            return Optional.of(Jwts.parser().setSigningKey(JWTS_KEY).parseClaimsJws(authToken).getBody());
        } catch (SignatureException | MalformedJwtException e) {
            return Optional.empty();
        }
    }

    public String removeBearerPrefixFromToken(String token) {
        return token.substring(7);
    }

}
