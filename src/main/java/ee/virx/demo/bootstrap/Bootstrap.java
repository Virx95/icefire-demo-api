package ee.virx.demo.bootstrap;

import ee.virx.demo.domain.user.User;
import ee.virx.demo.domain.user.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Bootstrap implements ApplicationRunner {

    private UserRepository userRepository;
    private BCryptPasswordEncoder passwordEncoder;

    public Bootstrap(UserRepository userRepository, BCryptPasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        userRepository.save(new User("user", passwordEncoder.encode("password")));
        userRepository.save(new User("alice", passwordEncoder.encode("password")));
        userRepository.save(new User("bob", passwordEncoder.encode("password")));
    }
}
