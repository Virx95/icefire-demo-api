package ee.virx.demo.controller;


import ee.virx.demo.controller.vm.EncryptVM;
import ee.virx.demo.exception.CustomExceptionHandler;
import ee.virx.demo.service.EncryptionService;
import ee.virx.demo.service.dto.EncryptionDTO;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class EncryptionController implements CustomExceptionHandler {

    private EncryptionService encryptionService;

    public EncryptionController(EncryptionService encryptionService) {
        this.encryptionService = encryptionService;
    }

    @PutMapping("/encrypt")
    public EncryptionDTO encrypt(@Valid @RequestBody EncryptVM encryptVM) {
        return encryptionService.encryptMessage(encryptVM);
    }

    @GetMapping("/encrypt")
    public List<EncryptionDTO> getEncryptions() {
        return encryptionService.getAllUserEncryptedMessages();
    }

    @GetMapping("/encrypt/{id}")
    @PreAuthorize("@encryptionService.hasAccess(#id)")
    public EncryptionDTO getEncryptions(@PathVariable long id) {
        return encryptionService.getEncryptedMessage(id);
    }

    @GetMapping("/decrypt/{id}")
    @PreAuthorize("@encryptionService.hasAccess(#id)")
    public EncryptionDTO decrypt(@PathVariable long id) {
        return encryptionService.decryptMessage(id);
    }

}
