package ee.virx.demo.controller;

import ee.virx.demo.controller.vm.LoginResponseVM;
import ee.virx.demo.controller.vm.UserVM;
import ee.virx.demo.exception.CustomExceptionHandler;
import ee.virx.demo.service.UserService;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping(value = "/user")
public class UserController implements CustomExceptionHandler {

    private UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/login")
    public LoginResponseVM login(@Valid @RequestBody UserVM userVM) {
        return userService.loginUser(userVM);
    }
}
