package ee.virx.demo.controller.vm;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class EncryptVM {

    @NotBlank(message = "Message cannot be blank")
    private String message;

}
