package ee.virx.demo.controller.vm;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class LoginResponseVM {

    private String token;

}
