package ee.virx.demo.controller.vm;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class UserVM {

    @NotBlank(message="Username cannot be blank")
    private String username;

    @NotBlank(message="Password cannot be blank")
    private String password;

}
