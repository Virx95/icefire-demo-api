package ee.virx.demo.domain.encryption;

import ee.virx.demo.domain.user.User;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@NoArgsConstructor
public class Encryption {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;

    private String message;

    @ManyToOne
    private User user;

    public Encryption(String message, User user) {
        this.message = message;
        this.user = user;
    }
}
