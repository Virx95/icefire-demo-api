package ee.virx.demo.domain.encryption;

import ee.virx.demo.domain.user.User;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface EncryptionRepository extends CrudRepository<Encryption, Long> {

    List<Encryption> findAllByUser(User user);

}
