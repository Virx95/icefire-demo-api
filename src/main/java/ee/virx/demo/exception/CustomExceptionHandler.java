package ee.virx.demo.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

public interface CustomExceptionHandler {

    @ExceptionHandler
    @ResponseStatus(HttpStatus.CONFLICT)
    default JsonExceptionResponse handleProcessedDetailedDataException(ProcessingException e) {
        return new JsonExceptionResponse(e.getMessage());
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.CONFLICT)
    default JsonExceptionResponse handleMethodArgumentNotValidException(MethodArgumentNotValidException e) {
        FieldError err = e.getBindingResult().getFieldError();
        return new JsonExceptionResponse(err.getDefaultMessage());
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.FORBIDDEN)
    default JsonExceptionResponse handleAccessDeniedException(AccessDeniedException e) {
        return new JsonExceptionResponse(e.getMessage());
    }

    @ExceptionHandler
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    default JsonExceptionResponse handleHttpMessageNotReadableException(HttpMessageNotReadableException e) {
        return new JsonExceptionResponse("Please fill all fields");
    }
}
