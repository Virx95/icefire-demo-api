package ee.virx.demo.service;

import ee.virx.demo.domain.user.User;


public interface AccessControllableService {

    default boolean hasAccess(long entityId) {
        return new AuthenticatedUserAwareService().getAuthenticatedUser()
                .filter(user -> hasAccess(user, entityId))
                .isPresent();
    }

    boolean hasAccess(User authUser, long entityId);

}
