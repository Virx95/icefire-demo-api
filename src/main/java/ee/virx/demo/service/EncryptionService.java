package ee.virx.demo.service;

import ee.virx.demo.controller.vm.EncryptVM;
import ee.virx.demo.domain.encryption.Encryption;
import ee.virx.demo.domain.encryption.EncryptionRepository;
import ee.virx.demo.domain.user.User;
import ee.virx.demo.exception.ProcessingException;
import ee.virx.demo.service.dto.EncryptionDTO;
import ee.virx.demo.service.mapper.EncryptionMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class EncryptionService extends AuthenticatedUserAwareService implements AccessControllableService {

    private Encryptor encryptor;
    private EncryptionRepository encryptionRepository;
    private EncryptionMapper encryptionMapper;

    public EncryptionService(EncryptionRepository encryptionRepository) {
        this.encryptor = new Encryptor();
        this.encryptionRepository = encryptionRepository;
        this.encryptionMapper = new EncryptionMapper();
    }

    public EncryptionDTO encryptMessage(EncryptVM encryptVM) {
        return getAuthenticatedUser()
                .map(user -> encryptionRepository.save(new Encryption(encryptor.encrypt(encryptVM.getMessage(), user.getPassword()), user)))
                .map(encryptionMapper::encryptionToEncryptionDTO)
                .orElseThrow(() -> new ProcessingException("No user in security context"));
    }

    public EncryptionDTO decryptMessage(long encryptionId) {
        return encryptionRepository.findById(encryptionId)
                .map(encryption -> encryptor.decrypt(encryption.getMessage(), encryption.getUser().getPassword()))
                .map(message -> encryptionMapper.encryptionToEncryptionDTO(message, encryptionId))
                .orElseThrow(() -> new ProcessingException("No encrypted message"));
    }

    public List<EncryptionDTO> getAllUserEncryptedMessages() {
        return getAuthenticatedUser()
                .map(encryptionRepository::findAllByUser)
                .orElseThrow(() -> new ProcessingException("No user in security context"))
                .stream()
                .map(encryptionMapper::encryptionToEncryptionDTO)
                .collect(Collectors.toList());
    }

    public EncryptionDTO getEncryptedMessage(long id) {
        return encryptionRepository.findById(id)
                .map(encryptionMapper::encryptionToEncryptionDTO)
                .orElseThrow(() -> new ProcessingException("No encryption message found with id: " + id ));
    }

    @Override
    public boolean hasAccess(User authUser, long entityId) {
        return encryptionRepository.findById(entityId)
                .orElseThrow(() -> new ProcessingException("No encrypted message found with id: " + entityId))
                .getUser().getId() == authUser.getId();
    }
}
