package ee.virx.demo.service;

import org.jasypt.util.text.AES256TextEncryptor;


public class Encryptor {

    public String encrypt(String message, String secret) {
        return getTextEncryptor(secret).encrypt(message);
    }

    public String decrypt(String message, String secret) {
        return getTextEncryptor(secret).decrypt(message);
    }

    private AES256TextEncryptor getTextEncryptor(String secret) {
        AES256TextEncryptor textEncryptor = new AES256TextEncryptor();
        textEncryptor.setPassword(secret);
        return textEncryptor;
    }

}
