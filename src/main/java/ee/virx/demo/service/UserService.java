package ee.virx.demo.service;

import ee.virx.demo.controller.vm.LoginResponseVM;
import ee.virx.demo.exception.ProcessingException;
import ee.virx.demo.authentication.TokenService;
import ee.virx.demo.controller.vm.UserVM;
import ee.virx.demo.domain.user.User;
import ee.virx.demo.domain.user.UserRepository;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    private UserRepository userRepository;
    private TokenService tokenService;
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserService(UserRepository userRepository,
                       TokenService tokenService,
                       BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.tokenService = tokenService;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public LoginResponseVM loginUser(UserVM userVM) {
        User user = userRepository.findByUsername(userVM.getUsername())
                .orElseThrow(() -> new ProcessingException("Invalid credentials"));

        if (userPasswordMatchesHash(userVM.getPassword(), user.getPassword())) {
            return tokenService.generateTokenResponse(user);
        } else {
            throw new ProcessingException("Invalid credentials");
        }
    }

    private boolean userPasswordMatchesHash(String password, String passwordHash) {
        if (passwordHash == null) {
            return false;
        }
        return bCryptPasswordEncoder.matches(password, passwordHash);
    }

}
