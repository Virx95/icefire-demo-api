package ee.virx.demo.service.dto;

import lombok.Data;

@Data
public class EncryptionDTO {

    private long id;

    private String message;

    public EncryptionDTO(long id, String message) {
        this.id = id;
        this.message = message;
    }
}
