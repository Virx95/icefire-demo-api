package ee.virx.demo.service.mapper;

import ee.virx.demo.domain.encryption.Encryption;
import ee.virx.demo.service.dto.EncryptionDTO;

public class EncryptionMapper {

    public EncryptionDTO encryptionToEncryptionDTO(Encryption encryption) {
        return new EncryptionDTO(encryption.getId(), encryption.getMessage());
    }

    public EncryptionDTO encryptionToEncryptionDTO(String message, long id) {
        return new EncryptionDTO(id, message);
    }

}
