package ee.virx.demo.authentication;

import ee.virx.demo.controller.vm.LoginResponseVM;
import ee.virx.demo.domain.user.User;
import ee.virx.demo.domain.user.UserRepository;
import io.jsonwebtoken.Claims;
import org.junit.Before;
import org.junit.Test;

import java.util.Optional;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TokenServiceTest {

    private TokenService tokenService;
    private UserRepository userRepositoryMock;

    @Before
    public void setUp() throws Exception {
        userRepositoryMock = mock(UserRepository.class);
        tokenService = new TokenService(userRepositoryMock, "secret");
        tokenService.setJWTSKey();
    }

    @Test
    public void generateTokenTest() {
        LoginResponseVM loginResponseVM = tokenService.generateTokenResponse(new User("usr", "pass"));
        assertNotNull(loginResponseVM.getToken());
        assertFalse(loginResponseVM.getToken().isBlank());
    }

    @Test
    public void getUserFromTokenTest() {
        var username = "usr";
        var pass = "pass";
        LoginResponseVM loginResponseVM = tokenService.generateTokenResponse(getUser(username, pass));
        Optional<User> userFromTokenInfo = tokenService.getUserFromTokenInfo(loginResponseVM.getToken());
        assertEquals(username, userFromTokenInfo.get().getUsername());
        assertEquals(pass, userFromTokenInfo.get().getPassword());
    }

    @Test
    public void getTokenClaimsTest() {
        var username = "usr";
        var pass = "pass";

        LoginResponseVM loginResponseVM = tokenService.generateTokenResponse(getUser(username, pass));
        Optional<Claims> tokenClaims = tokenService.getTokenClaims(loginResponseVM.getToken());
        assertTrue(tokenClaims.isPresent());
        assertEquals("1", tokenClaims.get().getSubject());
    }

    @Test
    public void removeBearerPrefixTest() {
        User user = getUser("usr", "pass");
        LoginResponseVM loginResponseVM = tokenService.generateTokenResponse(user);
        String tokenWithBearerPrefix = "Bearer " + loginResponseVM.getToken();
        assertEquals(loginResponseVM.getToken(), tokenService.removeBearerPrefixFromToken(tokenWithBearerPrefix));
    }

    private User getUser(String username, String pass) {
        User user = new User(username, pass);
        user.setId(1);
        when(userRepositoryMock.findById(1L)).thenReturn(Optional.of(user));
        return user;
    }
}
