package ee.virx.demo.service;


import ee.virx.demo.domain.user.User;
import ee.virx.demo.service.AuthenticatedUserAwareService;
import org.junit.Test;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class AuthenticatedUserAwareServiceTest {

    @Test
    public void getUserFromSecurityContextTest() {
        AuthenticatedUserAwareService userAwareService = new AuthenticatedUserAwareService();
        assertFalse(userAwareService.getAuthenticatedUser().isPresent());
        pushUserToSecurityContext();
        assertTrue(userAwareService.getAuthenticatedUser().isPresent());
    }

    private void pushUserToSecurityContext() {
        User user = new User("usr", "pass");
        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                user, null, user.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
