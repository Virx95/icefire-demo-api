package ee.virx.demo.service;

import ee.virx.demo.controller.vm.EncryptVM;
import ee.virx.demo.domain.encryption.Encryption;
import ee.virx.demo.domain.encryption.EncryptionRepository;
import ee.virx.demo.domain.user.User;
import ee.virx.demo.exception.ProcessingException;
import ee.virx.demo.service.dto.EncryptionDTO;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class EncryptionServiceTest {

    private EncryptionService encryptionService;
    private EncryptionRepository encryptionRepositoryMock;

    @Before
    public void setUp() {
        encryptionRepositoryMock = mock(EncryptionRepository.class);
        encryptionService = spy(new EncryptionService(encryptionRepositoryMock));
    }

    @Test
    public void encryptMessageTest() {
        resetSecurityContext();
        var messageString = "my message to be encrypted";
        Encryption mockEncryption = getMockEncryption(messageString,2);
        var emcryptVM = new EncryptVM();
        emcryptVM.setMessage(messageString);
        EncryptionDTO encryptionDTO = encryptionService.encryptMessage(emcryptVM);
        assertFalse(encryptionDTO.getMessage().isBlank());
        assertEquals(mockEncryption.getMessage(), encryptionDTO.getMessage());
    }

    @Test(expected = ProcessingException.class)
    public void encryptMessageFailTest() {
        var messageString = "my message to be encrypted";
        var emcryptVM = new EncryptVM();
        emcryptVM.setMessage(messageString);
        encryptionService.encryptMessage(emcryptVM);
    }

    @Test
    public void decryptMessageTest() {
        var message = "my message";
        Encryption mockEncryption = getMockEncryption(message, 2);
        when(encryptionRepositoryMock.findById(2L)).thenReturn(Optional.ofNullable(mockEncryption));
        EncryptionDTO encryptionDTO = encryptionService.decryptMessage(2);
        assertEquals(message, encryptionDTO.getMessage());
    }

    @Test(expected = ProcessingException.class)
    public void decryptMessageFailTest() {
        encryptionService.decryptMessage(2);
    }

    @Test
    public void getAllUserEncryptedMessagesTest() {
        Encryption firstMessage = getMockEncryption("first message", 1);
        Encryption secondMessage = getMockEncryption("second message", 2);
        List<Encryption> allEncryptions = List.of(firstMessage, secondMessage);
        when(encryptionRepositoryMock.findAllByUser(any())).thenReturn(allEncryptions);

        List<EncryptionDTO> allUserEncryptedMessages = encryptionService.getAllUserEncryptedMessages();
        assertEquals(firstMessage.getMessage(), allUserEncryptedMessages.get(0).getMessage());
        assertEquals(secondMessage.getMessage(), allUserEncryptedMessages.get(1).getMessage());
    }

    @Test(expected = ProcessingException.class)
    public void getAllUserEncryptedMessagesFailTest() {
        resetSecurityContext();
       encryptionService.getAllUserEncryptedMessages();
    }

    @Test
    public void getEncryptedMessageTest() {
        var message = "my message";
        var id = 2;
        Encryption encryption = getMockEncryption(message, id);
        when(encryptionRepositoryMock.findById(any())).thenReturn(Optional.ofNullable(encryption));

        EncryptionDTO encryptedMessage = encryptionService.getEncryptedMessage(id);
        assertFalse(encryptedMessage.getMessage().isBlank());
        assertEquals(id, encryptedMessage.getId());
    }

    @Test(expected = ProcessingException.class)
    public void getEncryptedMessageFailTest() {
        resetSecurityContext();
        EncryptionDTO encryptedMessage = encryptionService.getEncryptedMessage(2);
    }

    private Encryption getMockEncryption(String messageToEncrypt, long id) {
        var user = pushUserToSecurityContex();
        var encryptor = new Encryptor();
        Encryption encryption = new Encryption(encryptor.encrypt(messageToEncrypt, user.getPassword()), user);
        encryption.setId(id);
        when(encryptionRepositoryMock.save(any())).thenReturn(encryption);
        return encryption;
    }

    private User pushUserToSecurityContex() {
        var user = new User("usr", "pass");
        user.setId(1);

        UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(
                user, null, user.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return user;
    }

    private void resetSecurityContext() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }
}
