package ee.virx.demo.service;

import ee.virx.demo.domain.encryption.Encryption;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class EncryptorTest {

    @Test
    public void encryptorTest() {
        Encryptor encryptor = new Encryptor();
        var message = "my message";
        var secret = "randomstring";

        String encrypt = encryptor.encrypt(message, secret);
        assertFalse(encrypt.isBlank());

        String decrypt = encryptor.decrypt(encrypt, secret);
        assertEquals(message, decrypt);
    }

}
