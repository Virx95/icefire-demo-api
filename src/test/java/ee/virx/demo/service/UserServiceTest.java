package ee.virx.demo.service;

import ee.virx.demo.authentication.TokenService;
import ee.virx.demo.controller.vm.LoginResponseVM;
import ee.virx.demo.controller.vm.UserVM;
import ee.virx.demo.domain.encryption.EncryptionRepository;
import ee.virx.demo.domain.user.User;
import ee.virx.demo.domain.user.UserRepository;
import ee.virx.demo.exception.ProcessingException;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Optional;

import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class UserServiceTest {

    private BCryptPasswordEncoder passwordEncoder;
    private UserService userService;
    private UserRepository userRepository;
    private TokenService tokenService;

    @Before
    public void setUp() {
        userRepository = mock(UserRepository.class);
        tokenService = new TokenService(userRepository, "secret");
        tokenService.setJWTSKey();
        passwordEncoder = new BCryptPasswordEncoder();
        userService = new UserService(userRepository, tokenService, passwordEncoder);
    }

    @Test
    public void loginUserTest() {
        var username = "user";
        var pass = "mysecretpassword";
        getUser(username, pass);

        UserVM userVM = new UserVM();
        userVM.setUsername(username);
        userVM.setPassword(pass);
        LoginResponseVM loginResponseVM = userService.loginUser(userVM);

        assertFalse(loginResponseVM.getToken().isBlank());
    }

    @Test(expected = ProcessingException.class)
    public void loginUserWrongPasswordTest() {
        var username = "user";
        var pass = "mysecretpassword";
        getUser(username, pass);

        UserVM userVM = new UserVM();
        userVM.setUsername(username);
        userVM.setPassword("wrongpass");
        userService.loginUser(userVM);
    }

    @Test(expected = ProcessingException.class)
    public void loginUserWrongUsernameTest() {
        var username = "user";
        var pass = "mysecretpassword";
        getUser(username, pass);

        UserVM userVM = new UserVM();
        userVM.setUsername("wrongusername");
        userService.loginUser(userVM);
    }

    private void getUser(String username, String pass) {
        User user = new User(username, passwordEncoder.encode(pass));
        user.setId(1);
        when(userRepository.findByUsername(username)).thenReturn(Optional.of(user));
    }

}
